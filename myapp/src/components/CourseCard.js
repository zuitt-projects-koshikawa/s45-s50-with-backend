import {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

	console.log(courseProp)
	// expected result is courseData[0]
	console.log(typeof courseProp)
	// result: object

	// const [count,setCount] = useState(0);
	// // syntax: const [getter, setter] = useState(initialValueOfGetter)
	// const [slots,setSlots] = useState(30);
	// // const [isOpen, setIsOpen] = useState(true);
	

	// function enroll () {
	// 	if (slots > 0) {
	// 			setSlots(slots - 1)
	// 			console.log('Enrollees ' - slots)
	// 			setCount(count + 1)
	// 			console.log('Enrollees ' + count)
	// 	} /*else {
	// 		return alert(`No more available slots for ${name}`)
	// 	}*/
	// }

	// useEffect(() => {
	// 	if (slots === 0) {
	// 		alert(`No more available slots for ${name}`)
	// 		// setIsOpen(false)
	// 	}

	// }, [slots])
	// // syntax: useEffect(() => {}, [])



	const {name, description, price, _id } = courseProp
	return (
		
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Button variant = "primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
					 </Card.Body>
				</Card>

	

	)
}
