//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const userRoutes = require('./routes/userRoutes');
	const courseRoutes = require('./routes/courseRoutes');


//[SECTION] Server Setup
	const app = express();
	const port = process.env.PORT || 4000;
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));
	app.use(cors());


//[SECTION] Database Connect
	mongoose.connect("mongodb+srv://admin_koshikawa:admin169@batch-169.df9yo.mongodb.net/react-booking?retryWrites=true&w=majority",
		{
			useNewUrlParser : true,
			useUnifiedTopology: true
		}
	);

	let db = mongoose.connection

		db.on('error', () => console.error.bind(console, 'error'))
		db.once('open', () => console.log('Successfully connected to MongoDB'))


//[SECTION] Server Routes
	app.use('/users', userRoutes);
	app.use('/courses', courseRoutes);

//[SECTION] Server Response
	app.listen(port, () => console.log(`Server is running at port ${port}`))